package mysql

import (
	"bitbucket.org/yarik_bratashchuk/testapp"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
)

// UserAuthService is a MySQL implementation of testapp.UserAuthSession
type UserAuthService struct {
	*DB
}

// SaveState implements temporary session creation with the state for the
// 3-legged oauth authentication flow. It just creates a record in
// a database with the unique state string (uuid)
func (as *UserAuthService) SaveState(state string) error {
	_, err := as.DB.Exec(fmt.Sprintf("INSERT INTO oauth_states (state) VALUES ('%s')", state))
	if err != nil {
		logger.Println(err)
		return err
	}
	return nil
}

// CheckState implements checking whether the state returned by provider
// authorisation page is valid by checking if the returned state
// exist in the database (it is uuid)
func (as *UserAuthService) CheckState(state string) error {
	var n int
	err := as.DB.QueryRow(fmt.Sprintf(
		"SELECT COUNT(*) FROM oauth_states WHERE state='%s'", state)).Scan(&n)
	if err != nil {
		logger.Println(err)
		return err
	}
	// we don't need state anymore
	as.DB.Exec(fmt.Sprintf("DELETE FROM oauth_states WHERE state='%s'", state))
	return nil
}

// SaveUserInfo handles saving user info retrieved from 3rd party
// api (linkedIn, google, facebook...)
func (as *UserAuthService) SaveUserInfo(u interface{}) error {
	switch u.(type) {
	case testapp.UserLinkedIn:
		if err := as.saveLinkedInUser(u.(testapp.UserLinkedIn)); err != nil {
			return err
		}
		return nil
	}
  return nil
}

// saveLinkedInUser is an internal function used for
// handling saving user data retrieved particularly from
// linkedIn
func (as *UserAuthService) saveLinkedInUser(u testapp.UserLinkedIn) error {
	fullName := fmt.Sprintf("%s %s", u.FirstName, u.LastName)

	_, err := as.DB.Exec(fmt.Sprintf(
		"INSERT INTO users (full_name, email, address) VALUES ('%s', '%s', '%s') ON DUPLICATE KEY UPDATE "+
			"full_name = VALUES(full_name), address = VALUES(address)", fullName, u.Email, u.Location.Name))
	if err != nil {
		logger.Println(err)
		return err
	}

	return nil
}
