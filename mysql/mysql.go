// Package mysql is an adaptor to mysql database
package mysql

import (
	"bitbucket.org/yarik_bratashchuk/testapp"
	"database/sql"
	"errors"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"golang.org/x/crypto/bcrypt"
	"log"
	"os"
)

var (
	logger *log.Logger
)

func init() {
	file, err := os.OpenFile(`db_errors.log`, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	logger = log.New(file, ``, log.Ldate|log.Ltime|log.Lshortfile)
}

type DB struct {
	*sql.DB
}

// UserService represents a MySQL implementation of testapp.UserService.
type UserService struct {
	*DB
}

// CreateUser creates a user in db.
func (s *UserService) CreateUser(email, password string) error {
	// common practice is to store hashed password
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), 10)
	if err != nil {
		logger.Println(err)
		return err
	}

	_, err = s.DB.Exec(fmt.Sprintf(
		"INSERT INTO users (password, email) VALUES ('%s', '%s')", hashedPassword, email))
	if err != nil {
		logger.Println(err)
		return err
	}

	return nil
}

// CheckUserCredentials checks if user exist (by email)
// and if the provided password is valid
func (s *UserService) CheckUserCredentials(user testapp.User) error {
	var hash string
	err := s.DB.QueryRow(fmt.Sprintf(
		"SELECT password FROM users WHERE email='%s'", user.Email)).Scan(&hash)
	if err != nil {
		logger.Println(err)
		return errors.New("User with this email is not registered")
	}

	if bcrypt.CompareHashAndPassword([]byte(hash), []byte(user.Password)) == nil {
		return nil
	}

	return errors.New("Wrong password")
}

// GetUser returns user
func (s *UserService) GetUser(email string) (user testapp.User, err error) {
	row := s.DB.QueryRow(fmt.Sprintf(
		"SELECT email, full_name, address, telephone FROM users WHERE email='%s'", email))
	err = row.Scan(&user.Email, &user.FullName, &user.Address, &user.Telephone)
	if err != nil {
		logger.Println(err)
		return testapp.User{}, err
	}
	return
}

// UpdateUser updates all user info except password
func (s *UserService) UpdateUser(email string, user testapp.User) error {
	query := fmt.Sprintf("UPDATE users SET full_name='%s', address='%s', telephone='%s'",
		user.FullName, user.Address, user.Telephone)
	if user.Email != "" {
		query = fmt.Sprintf("%s, email='%s' ", query, user.Email)
	}
	query = fmt.Sprintf("%s WHERE email='%s' LIMIT 1", query, email)

	_, err := s.DB.Exec(query)
	if err != nil {
		logger.Println(err)
		fmt.Print(query)
		return err
	}
	return nil
}

// CheckUser tests whether user with provided email exist
func (s *UserService) CheckUser(email string) error {
	var n int
	row := s.DB.QueryRow(fmt.Sprintf("SELECT COUNT(*) FROM users WHERE email='%s'", email))
	err := row.Scan(&n)
	if err != nil {
		logger.Println(err)
		return err
	}

	if n != 0 {
		return errors.New("User with this email aready exist")
	}
	return nil
}

// Open returns a DB reference for a data source.
func Open(dataSourceName string) (*DB, error) {
	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		return nil, err
	}
	return &DB{db}, nil
}
