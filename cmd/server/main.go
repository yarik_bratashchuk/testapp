package main

import (
	"log"
	"net/http"
	"os"

	"bitbucket.org/yarik_bratashchuk/testapp/mysql"
)

func main() {
	// Connect to database
	db, err := mysql.Open(os.Getenv("CLEARDB_DATABASE_URL"))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// mysql implementation of testapp UserService and UserAuthService
	us := &mysql.UserService{DB: db}
	as := &mysql.UserAuthService{DB: db}

	// Register handlers
	mux := http.NewServeMux()

	// Public endpoints
	mux.Handle("/api/v1/signup", SignupHandler(us))
	mux.Handle("/api/v1/login", LoginHandler(us))
	mux.Handle("/api/v1/verifyToken", VerifyTokenHandler())

	// oauth authentication flow endpoinst
	mux.HandleFunc("/oauth/linkedin_login", LoginLinkedInHandler(as))
	mux.HandleFunc("/oauth/linkedin_callback", CallbackLinkedInHandler(as))

	// Protected endpoints
	mux.HandleFunc("/api/v1/my/profile", validateToken(ProfileHandler(us)))

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), mux))
}
