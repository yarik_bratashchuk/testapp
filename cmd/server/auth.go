package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"bitbucket.org/yarik_bratashchuk/testapp"
)

type Key int

// key for the context value
// must be comparable
const MyKey Key = 0

// claims holds user email
type Claims struct {
	Email string `json:"email"`

	jwt.StandardClaims
}

// createToken creates a jwt token
func createToken(email string) (string, error) {
	expireToken := time.Now().Add(time.Hour * 1).Unix()
	mySigningKey := []byte(os.Getenv("SECRET"))

	// Create the claims
	claims := Claims{
		email,
		jwt.StandardClaims{
			ExpiresAt: expireToken,
			Issuer:    "testapp",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(mySigningKey)
}

// verifyToken does what you expect
func verifyToken(tokenString string) (*jwt.Token, error) {
	var claims Claims

	token, err := jwt.ParseWithClaims(tokenString, &claims, func(token *jwt.Token) (interface{}, error) {
		// Make sure token's signature wasn't changed
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("Unexpected siging method")
		}
		return []byte(os.Getenv("SECRET")), nil
	})
	if err != nil {
		return nil, err
	}

	if _, ok := token.Claims.(*Claims); ok && token.Valid {
		return token, nil
	} else {
		return nil, errors.New("Token is not valid")
	}
}

// middleware for protected endpoints
func validateToken(endpoint http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bearer := r.Header.Get(http.CanonicalHeaderKey("authorization"))
		if bearer == "" {
			response(w, "No token provided", "error", "")
			return
		}

		tokenString := strings.Split(bearer, " ")[1]

		// if token is not valid return error
		token, err := verifyToken(tokenString)
		if err != nil {
			response(w, err.Error(), "error", "")
			return
		}

		if claims, ok := token.Claims.(*Claims); ok && token.Valid {
			ctx := context.WithValue(context.Background(), MyKey, *claims)
			endpoint(w, r.WithContext(ctx))
		} else {
			http.NotFound(w, r)
			return
		}
	})
}

// SignupHandler creates handler for user registration
func SignupHandler(us testapp.UserService) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var user testapp.User

		if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
			response(w, err.Error(), "error", "")
			return
		}

		// check if user already exist
		if err := us.CheckUser(user.Email); err != nil {
			response(w, err.Error(), "error", "")
			return
		}

		if err := us.CreateUser(user.Email, user.Password); err != nil {
			response(w, err.Error(), "error", "")
			return
		}

		// TODO: email confirmation

		token, err := createToken(user.Email)
		if err != nil {
			log.Printf("Error creating token: %s", err.Error())
			http.Error(w, err.Error(), 500)
		}

		w.Header().Add(`Authorization`, fmt.Sprintf("Bearer %s", token))
		response(w, "", "success", "")
	}
	return http.HandlerFunc(fn)
}

// LoginHandler creates handler for user login
func LoginHandler(us testapp.UserService) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		var user testapp.User

		if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
			response(w, err.Error(), "error", "")
			return
		}

		// check if user exist and if his password is valid
		if err := us.CheckUserCredentials(user); err != nil {
			response(w, err.Error(), "error", "")
			return
		}

		token, err := createToken(user.Email)
		if err != nil {
			log.Printf("Error creating token: %s", err.Error())
			http.Error(w, err.Error(), 500)
		}

		w.Header().Add(`Authorization`, fmt.Sprintf("Bearer %s", token))
		response(w, "", "success", "")
	}
	return http.HandlerFunc(fn)
}

// VerifyTokenHandler does what you expect
func VerifyTokenHandler() http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		bearer := r.Header.Get(http.CanonicalHeaderKey("authorization"))
		if bearer == "" {
			response(w, "No token provided", "error", "")
			return
		}

		token := strings.Split(bearer, " ")[1]

		// if token is not valid return error
		if _, err := verifyToken(token); err != nil {
			response(w, err.Error(), "error", "")
			return
		}

		response(w, "", "success", "")
	})
}
