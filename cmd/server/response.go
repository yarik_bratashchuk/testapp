package main

import (
	"encoding/json"
	"fmt"
	"net/http"

	"bitbucket.org/yarik_bratashchuk/testapp"
)

// response is a helper function that performs writing
// json status/error to http response body
// if newEmail is not an empty string then authorization token
// will be refreshed to carry new custom claims
func response(w http.ResponseWriter, errorString, statusString, newEmail string) {
	w.Header().Add(`Content-Type`, `application/json`)
	if newEmail != "" {
		newToken, err := createToken(newEmail)
		if err != nil {
			http.Error(w, err.Error(), 500)
		}
		w.Header().Add(`Authorization`, fmt.Sprintf("Bearer %s", newToken))
	}
	d := json.NewEncoder(w)

	d.Encode(map[string]string{
		"error":  errorString,
		"status": statusString,
	})
}

// responseWithData is a helper function that
// performs writing json data to http response
//
// this is a just a temporary solution
// not a real life solution
func responseWithData(w http.ResponseWriter, data interface{}, status string) {
	w.Header().Add(`Content-Type`, `application/json`)
	d := json.NewEncoder(w)

	switch data.(type) {
	case testapp.User:
		d.Encode(map[string]interface{}{
			"user":   data.(testapp.User),
			"status": status,
		})
	}
}
