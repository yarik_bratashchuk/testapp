package main

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/yarik_bratashchuk/testapp"
)

// ProfileHandler handles finish signup page
func ProfileHandler(us testapp.UserService) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var user testapp.User

		claims, ok := r.Context().Value(MyKey).(Claims)
		if !ok {
			response(w, "Permission denied", "error", "")
			return
		}

		switch r.Method {
		case "GET":
			user, err := us.GetUser(claims.Email)
			if err != nil {
				response(w, err.Error(), "error", "")
				return
			}
			responseWithData(w, user, "success")
		case "POST":
			if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
				response(w, err.Error(), "error", "")
				return
			}
			if err := us.UpdateUser(claims.Email, user); err != nil {
				response(w, err.Error(), "error", "")
				return
			}
			// if email was changes then new token will be sent
			response(w, "", "success", user.Email)
		}
	}
}
