package main

import (
	"bitbucket.org/yarik_bratashchuk/testapp"
	"github.com/nu7hatch/gouuid"
	"net/http"
	"net/url"
  "fmt"
  "io/ioutil"
  "encoding/json"
)

// linkedIn app credentials
var clientId = "77u0ngpgm67y2n"
var clientSecret = "EcsVayUsCvSjJzr9"

// hardcoded redirect url, I was lazy to move it
// to separate config file
var redirectURI = "http://127.0.0.1:8080/oauth/linkedin_callback"

// LoginLinkedInHandler handles login via linkedIn
func LoginLinkedInHandler(as testapp.UserAuthSession) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// creating id (state for oauth)
		id, _ := uuid.NewV4()

		values := make(url.Values)
		values.Add("response_type", "code")
		values.Add("client_id", clientId)
		values.Add("redirect_uri", redirectURI)
		values.Add("scope", "r_basicprofile,r_emailaddress")
		values.Add("state", id.String())

		// temporary save state to db
		if err := as.SaveState(id.String()); err != nil {
			response(w, err.Error(), "error", "")
			return
		}

		http.Redirect(w, r, fmt.Sprintf(
			"https://www.linkedin.com/oauth/v2/authorization?%s",
			values.Encode(),
		), 302)
	}
	return http.HandlerFunc(fn)
}

// CallbackLinkedInHandler handles redirect from linkedIn authorization page
func CallbackLinkedInHandler(as testapp.UserAuthSession) http.HandlerFunc {
	fn := func(w http.ResponseWriter, r *http.Request) {
		state := r.FormValue("state")
		code := r.FormValue("code")

		// get the session, if no session return to the client via redirect with error parameter
		if err := as.CheckState(state); err != nil {
			http.Redirect(w, r, "http://127.0.0.1:3000/?error=invalid+state", 302)
			return
		}

		accessToken, err := getAccessToken(state, code)
		if err != nil {
			http.Redirect(w, r, "http://127.0.0.1:3000/?error=error+getting+access+token", 302)
			return
		}

		data, err := getUserInfo(accessToken)
		if err != nil {
			http.Redirect(w, r, "http://127.0.0.1:3000/?error=error+getting+profile+info", 302)
			return
		}

		if err := as.SaveUserInfo(data); err != nil {
			http.Redirect(w, r, "http://127.0.0.1:3000/?error=error+saving+user", 302)
			return
		}

		// creating JWT token for handling session
		newToken, _ := createToken(data.Email)

		http.Redirect(w, r, fmt.Sprintf("http://127.0.0.1:3000/?token=%s", newToken), 302)

	}
	return http.HandlerFunc(fn)
}

// getAccessToken returns access token
func getAccessToken(state, code string) (string, error) {
	values := make(url.Values)
	values.Add("client_id", clientId)
	values.Add("client_secret", clientSecret)
	values.Add("code", code)
	values.Add("grant_type", "authorization_code")
	values.Add("redirect_uri", redirectURI)

	response, err := http.PostForm("https://www.linkedin.com/oauth/v2/accessToken", values)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	bs, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}

	tokenMap := struct{
		Token string `json:"access_token"`
	}{}

	if err := json.Unmarshal(bs, &tokenMap); err != nil {
		return "", err
	}
	// here we're just ignoring expiration time for the token
	// because we will use in the next function call and only once
	return tokenMap.Token, nil
}

// getEmail makes authenticated call and returns basic user info
func getUserInfo(accessToken string) (testapp.UserLinkedIn, error) {
	req, _ := http.NewRequest("GET", "https://api.linkedin.com/v1/people/~:(email-address,first-name,last-name,location)?format=json", nil)
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", accessToken))

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return testapp.UserLinkedIn{}, err
	}
	defer res.Body.Close()

	var data testapp.UserLinkedIn

	err = json.NewDecoder(res.Body).Decode(&data)
	if err != nil {
		return testapp.UserLinkedIn{}, err
	}
	return data, nil
}
