// Package testapp implements the highest abstract level of test application.
package testapp

// User holds all user data
type User struct {
	ID        int
	Email     string `json:"email"`
	Password  string `json:"password"`
	FullName  string `json:"fullName"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
}

// UserService describes actions on User
type UserService interface {
	CreateUser(string, string) error
	UpdateUser(string, User) error
	GetUser(string) (User, error)
	CheckUser(string) error
	CheckUserCredentials(User) error
}

// UserLinkedId holds basic user info retrieved from
// linkedin api
type UserLinkedIn struct {
	Email     string `json:"emailAddress"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Location  struct {
		Name string `json:"name"`
	} `json:"location"`
}

// UserAuthSession describes actions to
// handle tempopary session while user is being
// authenticated via oauth provider
type UserAuthSession interface {
	CheckState(string) error
	SaveState(string) error
	SaveUserInfo(interface{}) error
}
